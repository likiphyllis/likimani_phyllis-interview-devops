To run locally:

1. pip3 install -r requirements.txt
2. python3 app.py 

this will launch the following:

Backend API: http://localhost:5000/api
Swagger UI: http://localhost:5000/api/ui/
SPA: http://localhost:5000/

HOMEWORK INSTRUCTIONS
---------------------
Goals:
  * Dockerize a simple application for local use
  * Deploy the dockerized application to the cloud
  * Modify the app to incorporate a re-architected data model

Consider yourself to be a new member of the development team for a simple CRUD application (python + flask).  You can consider your recruiter to be playing the role of your Dev Manager, who can answer any questions that you have. Feel free to use them.

Your first ticket (ticket number CRUD-1234) is as follows:

CRUD-1234:

“In order to simplify the development, packaging and installation of CRUD, we would like to ‘Dockerize’ the application. Developers at many different skill levels will be expected to work with this containerized application."

Please create a pull request to satisfy this ticket. You will receive feedback using our normal PR process after doing so - this gives you a chance to see how we work.


Your second ticket (# CRUD-5678):

"Now that we have a working app, we would like to deploy it to production using a cloud provider.   Describe how you would go about doing this.  Include any recommendations you may have on specific cloud providers, tooling, or anything else you think is relevant."

Your third ticket (# CRUD-9ABC)
"As you may have noticed, the persistence model of this app is somewhat lacking.  Our application architect has decided to implement a database backend to capture changes made by users to the application data.  Describe how you would modify your choices in steps 2 and 3 to incorporate the new database backend."



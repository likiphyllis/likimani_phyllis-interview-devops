import json
from flask import make_response, abort

# Data to serve with our API.  Loaded on first call to read()
CHARACTERS = None

def load_data():
    """ Load Character data
    """
    global CHARACTERS
    with open("data/characters.json", "r") as read_file:
        CHARACTERS = json.load(read_file)
        print (CHARACTERS)

# Create a handler for our read (GET) characters
def read_all():
    """
    This function responds to a request for /api/characters
    with the complete lists of characters

    :return:        sorted list of characters
    """
    # Initialize Character data, if needed
    if not CHARACTERS:
        load_data()

    #return [CHARACTERS[key] for key in sorted(CHARACTERS.keys())]
    return CHARACTERS

def read_one(name):
    """
    This function responds to a request for /api/characters/{name}
    with one matching person from people
    :param name:   name of character to find
    :return:        character matching last name
    """
    # Initialize Character data, if needed
    if not CHARACTERS:
        load_data()

    # Does the person exist in characters?
    if name in CHARACTERS:
        character = CHARACTERS.get(name)

    # otherwise, nope, not found
    else:
        abort(
            404, "Character named {name} not found".format(name=name)
        )

    return character


def create(character):
    """
    This function creates a new character in the people structure
    based on the passed in character data
    :param character:  character to create in people structure
    :return:        201 on success, 406 on character exists
    """
    # Initialize Character data, if needed
    if not CHARACTERS:
        load_data()

    name = character.get("name", None)
    house = character.get("house", None)
    actor = character.get("actor", None)
    weapon = character.get("weapon", None)

    # Does the character exist already?
    if name not in CHARACTERS and name is not None:
        CHARACTERS[name] = {
            "house": house,
            "actor": actor,
            "weapon": weapon,
        }
        return make_response(
            "{name} successfully created".format(name=name), 201
        )

    # Otherwise, they exist, that's an error
    else:
        abort(
            406,
            "Character with name {name} already exists".format(name=name),
        )


def update(name, character):
    """
    This function updates an existing character in the people structure
    :param name:   name of character to update in the people structure
    :param character:  character to update
    :return:        updated character structure
    """

    # Initialize Character data, if needed
    if not CHARACTERS:
        load_data()

    # Does the character exist in people?
    if name in CHARACTERS:
        CHARACTERS[name]["house"] = character.get("house")
        CHARACTERS[name]["actor"] = character.get("actor")
        CHARACTERS[name]["weapon"] = character.get("weapon")

        return CHARACTERS[name]

    # otherwise, nope, that's an error
    else:
        abort(
            404, "Character with name {name} not found".format(name=name)
        )


def delete(name):
    """
    This function deletes a character from the people structure
    :param name:   last name of character to delete
    :return:        200 on successful delete, 404 if not found
    """

    # Initialize Character data, if needed
    if not CHARACTERS:
        load_data()

    # Does the character to delete exist?
    if name in CHARACTERS:
        del CHARACTERS[name]
        return make_response(
            "{name} successfully deleted".format(name=name), 200
        )

    # Otherwise, nope, character to delete not found
    else:
        abort(
            404, "Character with last name {name} not found".format(name=name)
        )

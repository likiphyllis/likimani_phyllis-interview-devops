/*
 * JavaScript file for the application to demonstrate
 * using the API
 */

// Create the namespace instance
let ns = {};

// Create the model instance
ns.model = (function() {
    'use strict';

    let $event_pump = $('body');

    // Return the API
    return {
        'read': function() {
            let ajax_options = {
                type: 'GET',
                url: 'api/characters',
                accepts: 'application/json',
                dataType: 'json'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_read_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        create: function(name, house, actor, weapon) {
            let ajax_options = {
                type: 'POST',
                url: 'api/characters',
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'name': name,
                    'house': house,
                    'actor': actor,
                    'weapon': weapon
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_create_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        update: function(name, house, actor, weapon) {
            let ajax_options = {
                type: 'PUT',
                url: 'api/characters/' + name,
                accepts: 'application/json',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify({
                    'house': house,
                    'actor': actor,
                    'weapon': weapon
                })
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_update_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        },
        'delete': function(name) {
            let ajax_options = {
                type: 'DELETE',
                url: 'api/characters/' + name,
                accepts: 'application/json',
                contentType: 'plain/text'
            };
            $.ajax(ajax_options)
            .done(function(data) {
                $event_pump.trigger('model_delete_success', [data]);
            })
            .fail(function(xhr, textStatus, errorThrown) {
                $event_pump.trigger('model_error', [xhr, textStatus, errorThrown]);
            })
        }
    };
}());

// Create the view instance
ns.view = (function() {
    'use strict';

    let $name = $('#name'),
        $house = $('#house'),
        $actor = $('#actor'),
        $weapon = $('#weapon');

    // return the API
    return {
        reset: function() {
            $name.val('');
            $house.val('');
            $actor.val('');
            $weapon.val('').focus();
        },
        update_editor: function(fname, lname) {
            $name.val(name);
            $house.val(val);
            $actor.val(actor);
            $weapon.val(weapon).focus();
        },
        build_table: function(characters) {
            let rows = ''

            // clear the table
            $('.characters table > tbody').empty();

            // did we get a characters array?
            if (characters) {
                /*for (let i=0, l=characters.length; i < l; i++) {*/
                for (var character in characters) {
                    rows += `<tr><td class="name">${character}</td><td class="house">${characters[character].house}</td><td>${characters[character].actor}</td><td>${characters[character].weapon}</td></tr>`;
                }
                $('table > tbody').append(rows);
            }
        },
        error: function(error_msg) {
            $('.error')
                .text(error_msg)
                .css('visibility', 'visible');
            setTimeout(function() {
                $('.error').css('visibility', 'hidden');
            }, 3000)
        }
    };
}());

// Create the controller
ns.controller = (function(m, v) {
    'use strict';

    let model = m,
        view = v,
        $event_pump = $('body'),
        $name = $('#name'),
        $house = $('#house'),
        $actor = $('#actor'),
        $weapon = $('#weapon');

    // Get the data from the model after the controller is done initializing
    setTimeout(function() {
        model.read();
    }, 100)

    // Validate input
    function validate(fname, lname) {
        return fname !== "" && lname !== "";
    }

    // Create our event handlers
    $('#create').click(function(e) {
        let name = $name.val(),
            house = $house.val(),
            actor = $actor.val(),
            weapon = $weapon.val();

        e.preventDefault();

        if (validate(name, house, actor, weapon)) {
            model.create(name, house, actor, weapon)
        } else {
            alert('Problem with input');
        }
    });

    $('#update').click(function(e) {
        let name = $name.val(),
            house = $house.val(),
            actor = $actor.val(),
            weapon = $weapon.val();

        e.preventDefault();

        if (validate(name, house, actor, weapon)) {
            model.update(name, house, actor, weapon)
        } else {
            alert('Problem with input');
        }
        e.preventDefault();
    });

    $('#delete').click(function(e) {
        let name = $name.val();

        e.preventDefault();

        if (validate('placeholder', name)) {
            model.delete(name)
        } else {
            alert('Problem with input');
        }
        e.preventDefault();
    });

    $('#reset').click(function() {
        view.reset();
    })

    $('table > tbody').on('dblclick', 'tr', function(e) {
        let $target = $(e.target),
            fname,
            lname;

        fname = $target
            .parent()
            .find('td.fname')
            .text();

        lname = $target
            .parent()
            .find('td.lname')
            .text();

        view.update_editor(fname, lname);
    });

    // Handle the model events
    $event_pump.on('model_read_success', function(e, data) {
        view.build_table(data);
        view.reset();
    });

    $event_pump.on('model_create_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_update_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_delete_success', function(e, data) {
        model.read();
    });

    $event_pump.on('model_error', function(e, xhr, textStatus, errorThrown) {
        let error_msg = textStatus + ': ' + errorThrown + ' - ' + xhr.responseJSON.detail;
        view.error(error_msg);
        console.log(error_msg);
    })
}(ns.model, ns.view));

